from django.shortcuts import render, reverse

from django.views.generic import (ListView, CreateView, DeleteView, UpdateView)

from hospital.models import Services, DoctorServices, DoctorTimeSlots, Appointments


# Create your views here.

def home(request):
    return render(request, 'hospital/index.html')


# ------------------------------------------------------
# Services Views
# ------------------------------------------------------

# The view to create a service, it will inherit from the django built in CreateView
class ServicesCreateView(CreateView):
    # it will act on the database table "Services"
    model = Services
    # It will require the following fields
    fields = ['name', 'description']
    # It will display the user creation form generated by the view on the "user_detail.html"
    template_name = 'hospital/services_detail.html'

    # The url to be redirected to after a new service is successfully added
    def get_success_url(self):
        # redirect user back to the page displaying a list of courses
        return reverse('services-list')

    # data is passed to the HTML/templates page in a context
    # override this method to get access to the context
    # and its data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add/create a new context variable called "table_title"
        # this variable can then be accessed on the template
        context['table_title'] = 'Add New Service'
        return context


class ServicesListView(ListView):
    model = Services
    template_name = 'hospital/services_list.html'
    context_object_name = 'services'


# The view to be used to delete a service, it will accept the "pk" as the variable holding the user_id
# it will inherit from the django built in DeleteView, it will perform the operation on the Services
# database table and will request a user to confirm the deletion operation on the confirm_delete.html
# It will finally state the url where a user is redirected after a successful deletion
class ServicesDeleteView(DeleteView):
    model = Services
    success_url = '/'
    template_name = 'users/confirm_delete.html'

    # overrides this method so as to add custom data to the context object that will be pushed to the
    # HTML page displaying the data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete Service'
        name = Services.objects.get(pk=self.kwargs.get('pk')).name
        context['message'] = f'Are you sure you want to delete the service "{name}"'
        context['cancel_url'] = 'services-list'
        return context


# This view will be used to update the patient details
class ServicesUpdateView(UpdateView):
    model = Services
    fields = ['name', 'description']
    template_name = 'hospital/services_detail.html'

    def get_success_url(self):
        return reverse('services-list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['table_title'] = 'Update Services'
        return context


# ------------------------------------------------------
# Doctor Services Views
# ------------------------------------------------------

# The view to create a doctor's services, it will inherit from the django built in CreateView
class DoctorServicesCreateView(CreateView):
    # it will act on the database table "DoctorServices"
    model = DoctorServices
    # It will require the following fields
    fields = ['service', 'doctor']
    # It will display the form generated by the view on the "doctor_services_detail.html"
    template_name = 'hospital/doctor_services_detail.html'

    # The url to be redirected to after a doctor has been linked to a service
    def get_success_url(self):
        # redirect user back to the page displaying a list of doctor services
        return reverse('doctor-services-list')

    # data is passed to the HTML/templates page in a context
    # override this method to get access to the context
    # and its data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add/create a new context variable called "table_title"
        # this variable can then be accessed on the template
        context['table_title'] = 'Link Doctor to a Service'
        return context


# The view to display the list of doctor's services, it will inherit from the django built in ListView
# it will get its data from the DoctorTimeSlots database table and display the data to the doctor_services_list.html
# page and data will be accessed on the page using the "doctor_services" variable object
class DoctorServicesListView(ListView):
    model = DoctorServices
    template_name = 'hospital/doctor_services_list.html'
    context_object_name = 'doctor_services'


# The view to be used to delete a doctors' services, it will accept the "pk" as the variable holding the user_id
# it will inherit from the django built in DeleteView, it will perform the operation on the DoctorServices
# database table and will request a user to confirm the deletion operation on the confirm_delete.html
# It will finally state the url where a user is redirected after a successful deletion
class DoctorServicesDeleteView(DeleteView):
    model = DoctorServices
    template_name = 'users/confirm_delete.html'

    def get_success_url(self):
        return reverse('doctor-services-list')

    # overrides this method so as to add custom data to the context object that will be pushed to the
    # HTML page displaying the data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = "Delete Doctor's Services"
        doctor_service = DoctorServices.objects.get(pk=self.kwargs.get('pk'))
        context[
            'message'] = f"Are you sure you want to delete Dr. {doctor_service.doctor.get_full_name()}'s {doctor_service.service.name} service? "
        context['cancel_url'] = 'doctor-services-list'
        return context


# This view will be used to update the doctors' services details
class DoctorServicesUpdateView(UpdateView):
    model = DoctorServices
    fields = ['service', 'doctor']
    template_name = 'hospital/doctor_services_detail.html'

    def get_success_url(self):
        return reverse('doctor-services-list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['table_title'] = "Update Doctor's Services"
        return context


# ------------------------------------------------------
# Doctor Time Slots Views
# ------------------------------------------------------


# The view to create a doctors' time slots, it will inherit from the django built in CreateView
class DoctorTimeSlotsCreateView(CreateView):
    # it will act on the database table "DoctorTimeSlots"
    model = DoctorTimeSlots
    # It will require the following fields
    fields = ['doctor_service', 'start_date', 'end_date']
    # It will display the user creation form generated by the view on the "doctor_time_slots_detail.html"
    template_name = 'hospital/doctor_time_slots_detail.html'

    # The url to be redirected to after a new doctors' time slots is successfully created
    def get_success_url(self):
        # redirect user back to the page displaying a list of doctor time slots
        return reverse('doctor-time-slots-list')

    # data is passed to the HTML/templates page in a context
    # override this method to get access to the context
    # and its data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add/create a new context variable called "table_title"
        # this variable can then be accessed on the template
        context['table_title'] = 'Add New Doctor Time Slot'
        return context


# The view to display the list of doctors' time slots, it will inherit from the django built in ListView
# it will get its data from the DoctorTimeSlots database table and display the data to the doctor_time_slots_list.html
# page and data will be accessed on the page using the "doctortimeslots" variable object
class DoctorTimeSlotsListView(ListView):
    model = DoctorTimeSlots
    template_name = 'hospital/doctor_time_slots_list.html'
    context_object_name = 'doctor_time_slots'


# The view to be used to delete a patient, it will accept the "pk" as the variable holding the user_id
# it will inherit from the django built in DeleteView, it will perform the operation on the DoctorTimeSlots
# database table and will request a user to confirm the deletion operation on the confirm_delete.html
# It will finally state the url where a user is redirected after a successful deletion
class DoctorTimeSlotsDeleteView(DeleteView):
    model = DoctorTimeSlots
    template_name = 'users/confirm_delete.html'

    # The url to be redirected to after a new doctors' time slots is successfully created
    def get_success_url(self):
        # redirect user back to the page displaying a list of doctor time slots
        return reverse('doctor-time-slots-list')

    # overrides this method so as to add custom data to the context object that will be pushed to the
    # HTML page displaying the data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = "Delete Doctor's Time Slot"
        doctor_time_slot = DoctorTimeSlots.objects.get(pk=self.kwargs.get('pk'))
        context[
            'message'] = f"Are you sure you want to delete Dr. {doctor_time_slot.doctor_service.doctor.get_full_name()}'s {doctor_time_slot.doctor_service.service.name} service {doctor_time_slot.start_date} time slot? "
        context['cancel_url'] = 'doctor-time-slots-list'
        return context


# This view will be used to update the doctors' time slots details
class DoctorTimeSlotsUpdateView(UpdateView):
    model = DoctorTimeSlots
    fields = ['doctor_service', 'start_date', 'end_date']
    template_name = 'hospital/doctor_time_slots_detail.html'

    def get_success_url(self):
        return reverse('doctor-time-slots-list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['table_title'] = "Update Doctor's Time Slot"
        return context


# ------------------------------------------------------
# Appointments Views
# ------------------------------------------------------


# The view to create an appointment, it will inherit from the django built in CreateView
class AppointmentsCreateView(CreateView):
    # it will act on the database table "Appointments"
    model = Appointments
    # It will require the following fields
    fields = ['patient', 'doctor_time_slots']
    # It will display the user creation form generated by the view on the "appointments_detail.html"
    template_name = 'hospital/appointments_detail.html'

    # The url to be redirected to after an appointment is successfully added
    def get_success_url(self):
        # redirect user back to the page displaying a list of appointments
        return reverse('appointments-list')

    # data is passed to the HTML/templates page in a context
    # override this method to get access to the context
    # and its data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add/create a new context variable called "table_title"
        # this variable can then be accessed on the template
        context['table_title'] = 'Add New Appointments'
        return context


# The view to display the list of appointments, it will inherit from the django built in ListView
# it will get its data from the Doctors database table and display the data to the appointments_list.html
# page and data will be accessed on the page using the "appointments" variable object
class AppointmentsListView(ListView):
    model = Appointments
    template_name = 'hospital/appointments_list.html'
    context_object_name = 'appointments'


# The view to be used to delete an appointment, it will accept the "pk" as the variable holding the user_id
# it will inherit from the django built in DeleteView, it will perform the operation on the Appointments
# database table and will request a user to confirm the deletion operation on the confirm_delete.html
# It will finally state the url where a user is redirected after a successful deletion
class AppointmentsDeleteView(DeleteView):
    model = Appointments
    template_name = 'users/confirm_delete.html'

    def get_success_url(self):
        return reverse('appointments-list')

    # overrides this method so as to add custom data to the context object that will be pushed to the
    # HTML page displaying the data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete Appointment'
        appointment = Appointments.objects.get(pk=self.kwargs.get('pk'))
        context['message'] = f'Are you sure you want to delete the "{appointment}"'
        context['cancel_url'] = 'appointments-list'
        return context


# This view will be used to update an appointment's details
class AppointmentsUpdateView(UpdateView):
    model = Appointments
    fields = ['patient', 'doctor_time_slots']
    template_name = 'hospital/appointments_detail.html'

    def get_success_url(self):
        return reverse('appointments-list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['table_title'] = 'Update Appointments'
        return context
