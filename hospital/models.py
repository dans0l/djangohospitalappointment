from django.db import models
from django.utils import timezone
from users.models import Doctors, Patients


# Create your models here.

# This table will hold the names of the services offered by the hospital and a brief description
class Services(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=500, default="Enter a brief description of the service offered")

    def __str__(self):
        return f'{self.name}'


# This table will hold the services held by each doctor
class DoctorServices(models.Model):
    # the medical service the doctor performs
    service = models.ForeignKey(Services, on_delete=models.PROTECT)
    doctor = models.ForeignKey(Doctors, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.doctor.get_full_name()} : {self.service.name}'


# This table will hold the timeslots when each doctor is available for a particular service
class DoctorTimeSlots(models.Model):
    # The doctor and the service he will offer
    doctor_service = models.ForeignKey(DoctorServices, on_delete=models.PROTECT)
    # The date he will be available to provide the service
    start_date = models.DateField(default=timezone.now)
    end_date = models.DateField(default=timezone.now)

    def __str__(self):
        return f'{self.doctor_service.doctor.get_full_name()} {self.start_date} Time Slot'


# This table will hold the appointments made, the patient that has booked a specific time slot
class Appointments(models.Model):
    # By not placing a UNIQUE constraint on the field below, we are allowing multiple users to book
    # appointment at the same time slot
    doctor_time_slots = models.ForeignKey(DoctorTimeSlots, on_delete=models.PROTECT)
    patient = models.ForeignKey(Patients, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.patient.get_full_name()} {self.doctor_time_slots.start_date} Appointment'
