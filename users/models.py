from django.db import models
from django.contrib.auth.models import User


# Create your models here.

# Create a Doctor and Patient tables that inherit from
# the User table we are doing this so that we can add a new field
# not included in the User table

class Doctors(User):
    # the patient's phone number
    phone_number = models.CharField(default="+254", unique=True, max_length=15)

    def __str__(self):
        return f'{self.get_full_name()}'


class Patients(User):
    # the patient's phone number
    phone_number = models.CharField(default="+254", unique=True, max_length=15)

    def __str__(self):
        return f'{self.get_full_name()}'
